package com.example.mohammad.sessiontwo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button openSecondActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondactivity_test);
        openSecondActivity = (Button)findViewById(R.id.openSecondActivity);
        openSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent secondIntent = new Intent(MainActivity.this , secoundActivity.class);
                secondIntent.putExtra("name" ,"Mohadese");
                secondIntent.putExtra("age" , 25);
                startActivity(secondIntent);
                finish();

            }
        });





    }
}
